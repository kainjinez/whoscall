package net.truonghieu.aigoido.base;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by quanghieu on 2/4/17.
 */

public class App extends Application {

    static App instance;

    public App() {
        this.instance = this;
    }

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        RealmConfiguration myConfig = new RealmConfiguration.Builder()
                .name("agd.db")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(myConfig);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
