package net.truonghieu.aigoido.ui.fragment.calllog;

import net.truonghieu.aigoido.model.PhoneNumber;
import net.truonghieu.aigoido.ui.fragment.calllog.base.BaseCallLogFragment;

import java.util.Calendar;

/**
 * Created by quanghieu on 4/8/17.
 */

public class CallLogSearchFragment extends BaseCallLogFragment {

    public static CallLogSearchFragment newInstance(){
        CallLogSearchFragment historySearchFragment = new CallLogSearchFragment();
        return historySearchFragment;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void initDataSource() {
        for (int i = 0; i < 20; i++) {
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setPhoneNumber("098263158" + i);
            phoneNumber.setAddress("Hanoi");
            phoneNumber.setCarrier(i % 2 == 0 ? "VIETTEL" : null);
            phoneNumber.setName("Trương Quang Hiếu " + i);
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, 0 - i);
            c.add(Calendar.HOUR, i);
            phoneNumber.setTime(c.getTimeInMillis());
            listPhoneNumbers.add(phoneNumber);
        }
        callLogAdapter.notifyDataSetChanged();
    }
}
