package net.truonghieu.aigoido.ui.fragment.calllog;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.widget.Toast;

import net.truonghieu.aigoido.model.CallType;
import net.truonghieu.aigoido.model.PhoneNumber;
import net.truonghieu.aigoido.ui.fragment.calllog.base.BaseCallLogFragment;

import java.util.Date;

/**
 * Created by quanghieu on 4/8/17.
 */

public class DeviceCallLogFragment extends BaseCallLogFragment {

    public static DeviceCallLogFragment newInstance() {
        DeviceCallLogFragment deviceCallLogFragment = new DeviceCallLogFragment();
        return deviceCallLogFragment;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void initDataSource() {
        DeviceCallLogAsyncTask dev = new DeviceCallLogAsyncTask(getActivity());
        dev.execute();
    }

    private class DeviceCallLogAsyncTask extends AsyncTask<Void, Void, Void> {

        Context context;
        boolean isCorrupted;
        int corruptCode;

        public static final int PERMISSION_FAILED = 101;

        public DeviceCallLogAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            listPhoneNumbers.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                isCorrupted = true;
                corruptCode = PERMISSION_FAILED;
                return null;
            }
            Cursor c = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
            assert c != null;
            int number = c.getColumnIndex(CallLog.Calls.NUMBER);
            int callType = c.getColumnIndex(CallLog.Calls.TYPE);
            int date = c.getColumnIndex(CallLog.Calls.DATE);
            int name = c.getColumnIndex(CallLog.Calls.CACHED_NAME);

            String strNumber, strCallType, callDate, strName;
            long valueCallDate;
            int valueCallType;

            while (c.moveToNext()){
                strNumber = c.getString(number);
                strCallType = c.getString(callType);
                callDate = c.getString(date);
                strName = c.getString(name);

                valueCallDate = Long.valueOf(callDate);
                valueCallType = Integer.valueOf(strCallType);

                PhoneNumber phoneNumber = new PhoneNumber();

                phoneNumber.setPhoneNumber(strNumber);

                if (TextUtils.isEmpty(strName)){
                    phoneNumber.fromRealm();
                }else{
                    phoneNumber.setName(strName);
                }

                phoneNumber.setTime(valueCallDate);

                switch (valueCallType){
                    case CallLog.Calls.OUTGOING_TYPE:
                        phoneNumber.setCallType(CallType.OUTGOING);
                        break;
                    case CallLog.Calls.INCOMING_TYPE:
                        phoneNumber.setCallType(CallType.INCOMING);
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        phoneNumber.setCallType(CallType.MISSED);
                        break;
                }

                listPhoneNumbers.add(phoneNumber);
            }

            c.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            callLogAdapter.notifyDataSetChanged();
        }
    }
}
