package net.truonghieu.aigoido.ui.fragment.calllog.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.truonghieu.aigoido.R;
import net.truonghieu.aigoido.model.PhoneNumber;
import net.truonghieu.aigoido.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by quanghieu on 4/6/17.
 */

public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.CallLogViewHolder> {

    protected Context context;
    protected List<PhoneNumber> phoneNumbers;
    protected int expanded_position = -1;

    protected static final int CONTENT_TYPE = 0;
    protected static final int HEADER_TYPE = 1;

    public CallLogAdapter(Context context, List<PhoneNumber> phoneNumbers) {
        this.context = context;
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    public CallLogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.item_call_log, parent, false);
        return new CallLogViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CallLogViewHolder holder, int position) {
        final PhoneNumber phoneNumber = phoneNumbers.get(position);
        //header
        if (position == 0){
            holder.llItemCallLogHeader.setVisibility(View.VISIBLE);
            holder.tvTitleGroup.setText(phoneNumber.getDayInString(context));
        }else{
            PhoneNumber prevPhoneNumber = phoneNumbers.get(position - 1);
            if (phoneNumber.isSameDay(prevPhoneNumber.getTime())){
                holder.llItemCallLogHeader.setVisibility(View.GONE);
            }else{
                holder.llItemCallLogHeader.setVisibility(View.VISIBLE);
                holder.tvTitleGroup.setText(phoneNumber.getDayInString(context));
            }
        }

        if (position != phoneNumbers.size() - 1){
            PhoneNumber nextPhoneNumber = phoneNumbers.get(position + 1);
            if (!phoneNumber.isSameDay(nextPhoneNumber.getTime())){
                holder.bottom_separator.setVisibility(View.INVISIBLE);
            }else{
                holder.bottom_separator.setVisibility(View.VISIBLE);
            }
        }

        if (position == expanded_position) {
            holder.llTabDetail.animate().translationYBy(1000).start();
            holder.llTabDetail.setVisibility(View.VISIBLE);
            holder.llTabDetail.animate().cancel();
        } else {
            holder.llTabDetail.animate().translationYBy(-1000).start();
            holder.llTabDetail.setVisibility(View.GONE);
            holder.llTabDetail.animate().cancel();
        }
        holder.tvAvatar.setText(phoneNumber.getAvatarText());
        DrawableCompat.setTint(holder.rlAvatar.getBackground(), ContextCompat.getColor(context, phoneNumber.getAvatarText().equals("?") ? R.color.colorOrange : R.color.colorGreen));
        holder.tvPhoneNumber.setText(phoneNumber.getPhoneNumber());
        holder.tvPhoneName.setText(phoneNumber.getName());
        holder.tvCarrier.setText(phoneNumber.getCarrier());
        holder.tvTime.setText(phoneNumber.getTimeInString(context));
        holder.llMainInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.llTabDetail.getVisibility() == View.VISIBLE) {
                    holder.llTabDetail.setVisibility(View.GONE);
                    expanded_position = -1;
                } else {
                    int tmp = expanded_position;
                    CallLogAdapter.this.notifyItemChanged(tmp);
                    expanded_position = holder.getAdapterPosition();
                    holder.llTabDetail.setVisibility(View.VISIBLE);
                }
                CallLogAdapter.this.notifyItemChanged(holder.getAdapterPosition());
            }
        });
        holder.llCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.dialPhone(context, phoneNumber.getPhoneNumber());
            }
        });
        holder.llSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.addContact(context, phoneNumber.getPhoneNumber(), phoneNumber.getName());
            }
        });
        holder.llSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.sendSMS(context, phoneNumber.getPhoneNumber());
            }
        });
        holder.llDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return phoneNumbers.size();
    }

    protected class CallLogViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.tvAvatar)
        TextView tvAvatar;
        @Nullable
        @BindView(R.id.tvPhoneNumber)
        TextView tvPhoneNumber;
        @Nullable
        @BindView(R.id.ivCallState)
        ImageView ivCallState;
        @Nullable
        @BindView(R.id.tvPhoneName)
        TextView tvPhoneName;
        @Nullable
        @BindView(R.id.tvCarrier)
        TextView tvCarrier;
        @Nullable
        @BindView(R.id.tvTime)
        TextView tvTime;
        @Nullable
        @BindView(R.id.llMainInfo)
        LinearLayout llMainInfo;
        @Nullable
        @BindView(R.id.llTabDetail)
        LinearLayout llTabDetail;
        @Nullable
        @BindView(R.id.llCall)
        LinearLayout llCall;
        @Nullable
        @BindView(R.id.llSms)
        LinearLayout llSms;
        @Nullable
        @BindView(R.id.llSave)
        LinearLayout llSave;
        @Nullable
        @BindView(R.id.llDetail)
        LinearLayout llDetail;
        @Nullable
        @BindView(R.id.tvTitleGroup)
        TextView tvTitleGroup;
        @Nullable
        @BindView(R.id.llItemCallLogHeader)
        LinearLayout llItemCallLogHeader;
        @Nullable
        @BindView(R.id.bottom_separator)
        View bottom_separator;
        @Nullable
        @BindView(R.id.rlAvatar)
        RelativeLayout rlAvatar;

        protected CallLogViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
