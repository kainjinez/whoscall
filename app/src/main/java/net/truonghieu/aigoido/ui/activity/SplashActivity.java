package net.truonghieu.aigoido.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ToggleButton;

import net.truonghieu.aigoido.R;
import net.truonghieu.aigoido.base.BaseActivity;

public class SplashActivity extends BaseActivity {
    ToggleButton tbOnOff;
    String[] strs = new String[] {Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET, Manifest.permission.PROCESS_OUTGOING_CALLS, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG};

    @Override
    protected void initViewAction() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.PROCESS_OUTGOING_CALLS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, strs, 1);
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.i("hieu", "permission granted");
                }else{
                    Log.i("hieu", "permission blocked. try next time");
                }
                break;
        }
    }

    @Override
    protected void initView() {
        tbOnOff = (ToggleButton) findViewById(R.id.tbOnOff);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }
}
