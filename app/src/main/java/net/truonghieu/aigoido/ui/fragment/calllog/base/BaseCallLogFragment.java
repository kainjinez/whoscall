package net.truonghieu.aigoido.ui.fragment.calllog.base;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import net.truonghieu.aigoido.R;
import net.truonghieu.aigoido.base.BaseFragment;
import net.truonghieu.aigoido.model.PhoneNumber;
import net.truonghieu.aigoido.ui.fragment.calllog.adapter.CallLogAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * Created by quanghieu on 4/6/17.
 */

public abstract class BaseCallLogFragment extends BaseFragment {
    @BindView(R.id.rcvMain)
    protected RecyclerView rcvMain;
    protected CallLogAdapter callLogAdapter;
    protected List<PhoneNumber> listPhoneNumbers = new ArrayList<>();

    @Override
    protected int getLayout() {
        return R.layout.fragment_call_log;
    }

    @Override
    protected void initView() {
        initRcv();
        initViews();
    }

    protected abstract void initViews();

    private void initRcv() {
        callLogAdapter = new CallLogAdapter(getActivity(), listPhoneNumbers);
        rcvMain.setAdapter(callLogAdapter);
        rcvMain.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rcvMain.setHasFixedSize(false);

        initDataSource();
    }

    protected abstract void initDataSource();

    @Override
    protected void initViewAction() {

    }
}
