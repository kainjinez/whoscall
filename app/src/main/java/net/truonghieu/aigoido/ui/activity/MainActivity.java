package net.truonghieu.aigoido.ui.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import net.truonghieu.aigoido.R;
import net.truonghieu.aigoido.base.BaseActivity;
import net.truonghieu.aigoido.ui.activity.adapter.MainViewPagerAdapter;
import net.truonghieu.aigoido.ui.fragment.calllog.CallLogSearchFragment;
import net.truonghieu.aigoido.ui.fragment.calllog.DeviceCallLogFragment;

import butterknife.BindView;

/**
 * Created by quanghieu on 4/6/17.
 */

public class MainActivity extends BaseActivity {
    @BindView(R.id.tlMenu)
    TabLayout tlMenu;
    @BindView(R.id.vpgMain)
    ViewPager vpgMain;
    @BindView(R.id.tvSettings)
    TextView tvSettings;

    MainViewPagerAdapter mainViewPagerAdapter;

    @Override
    protected void initViewAction() {

    }

    @Override
    protected void initView() {
        setupViewPager();

    }

    private void setupViewPager() {
        mainViewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager(), this);
        vpgMain.setAdapter(mainViewPagerAdapter);
        vpgMain.setOffscreenPageLimit(2);
        tlMenu.setupWithViewPager(vpgMain);

        mainViewPagerAdapter.addPage(CallLogSearchFragment.newInstance(), getString(R.string.found_calllog).toUpperCase());
        mainViewPagerAdapter.addPage(DeviceCallLogFragment.newInstance(), getString(R.string.your_calllog).toUpperCase());
        mainViewPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }
}
