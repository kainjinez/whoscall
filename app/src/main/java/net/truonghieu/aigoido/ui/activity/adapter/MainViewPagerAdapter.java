package net.truonghieu.aigoido.ui.activity.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import net.truonghieu.aigoido.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quanghieu on 4/6/17.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {
    Context context;
    List<String> pageTitles;
    List<BaseFragment> pageFragments;

    public MainViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        pageTitles = new ArrayList<>();
        pageFragments = new ArrayList<>();
    }


    public void addPage(BaseFragment fragment, String title){
        pageFragments.add(fragment);
        pageTitles.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return pageFragments.get(position);
    }

    @Override
    public int getCount() {
        return pageFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles.get(position);
    }
}
