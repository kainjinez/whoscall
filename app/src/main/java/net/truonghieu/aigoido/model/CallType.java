package net.truonghieu.aigoido.model;

/**
 * Created by quanghieu on 4/8/17.
 */

public enum CallType {
    INCOMING, OUTGOING, MISSED;
}
