package net.truonghieu.aigoido.model;

import android.content.Context;
import android.text.TextUtils;

import net.truonghieu.aigoido.utils.RealmUtils;
import net.truonghieu.aigoido.utils.Utils;

import java.util.Calendar;

import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by quanghieu on 2/4/17.
 */

public class PhoneNumber extends RealmObject {

    String phoneNumber;
    String address;
    String name;
    String carrier;
    long time;
    String callType;

    public PhoneNumber() {
    }

    public PhoneNumber(String phoneNumber, String address, String name, String carrier, long time) {
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.name = name;
        this.carrier = carrier;
        this.time = time;
    }

    public CallType getCallType() {
        return CallType.valueOf(callType);
    }

    public void setCallType(CallType callType) {
        this.callType = callType.toString();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        if (TextUtils.isEmpty(name)) {
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarrier() {
        if (TextUtils.isEmpty(carrier)) {
            return "";
        }
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getAvatarText() {
        if (!TextUtils.isEmpty(name)) {
            return String.valueOf(name.charAt(0));
        }
        if (!TextUtils.isEmpty(carrier)) {
            return String.valueOf(carrier.charAt(0));
        }
        return String.valueOf('?');
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTimeInString(Context context) {
        return Utils.getTimeInString(context, time);
    }

    public String getDayInString(Context context) {
        return Utils.getDayInString(context, time);
    }

    public boolean isSameDay(long time) {
        Calendar currCal = Calendar.getInstance();
        currCal.setTimeInMillis(this.time);
        Calendar oppoCal = Calendar.getInstance();
        oppoCal.setTimeInMillis(time);
        if (currCal.get(Calendar.YEAR) == oppoCal.get(Calendar.YEAR) &&
                currCal.get(Calendar.MONTH) == oppoCal.get(Calendar.MONTH) &&
                currCal.get(Calendar.DAY_OF_MONTH) == oppoCal.get(Calendar.DAY_OF_MONTH)) {
            return true;
        }

        return false;
    }

    public boolean fromRealm(){
        RealmResults<PhoneNumber> l = RealmUtils.getListPhoneNumbers("phoneNumber", phoneNumber);
        if (l.size() == 0){
            return  false;
        }else{
            phoneNumber = l.first().phoneNumber;
            address = l.first().address;
            name = l.first().name;
            carrier = l.first().carrier;
            time = l.first().time;
            callType = l.first().callType;

            return true;
        }
    }
}
