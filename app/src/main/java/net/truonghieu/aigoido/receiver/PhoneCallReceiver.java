package net.truonghieu.aigoido.receiver;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import net.truonghieu.aigoido.R;
import net.truonghieu.aigoido.model.PhoneNumber;
import net.truonghieu.aigoido.utils.retrofit.CrawlDataAsync;
import net.truonghieu.aigoido.utils.retrofit.Utils;

import java.util.Date;

/**
 * Created by quanghieu on 2/4/17.
 */

public class PhoneCallReceiver extends BasePhoneCallReceiver {
    @Override
    protected void onIncomingCallReceived(final Context ctx, String number, Date start) {
        if (Utils.isPhoneNumberExisted(ctx, number)){
            return;
        }

        Log.i("hieu", "call received:s " + number);
        CrawlDataAsync crawlDataAsync = new CrawlDataAsync(new CrawlDataAsync.OnCompleteListener() {
            @Override
            public void onSuccess(PhoneNumber phoneNumber) {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx);
//                RemoteViews remoteViews = new RemoteViews(ctx.getPackageName(), R.layout.layout_notification);
                builder.setSmallIcon(R.drawable.callinfo);
                builder.setContentTitle(phoneNumber.getName() + "(" + phoneNumber.getPhoneNumber() + ")");
                builder.setContentText(ctx.getString(R.string.maybe_call_you));
//        builder.setContent(remoteViews);
                builder.setPriority(NotificationCompat.PRIORITY_MAX);
                builder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);

                Toast.makeText(ctx, phoneNumber.getName() + "(" + phoneNumber.getPhoneNumber() + ")"
                        + "\n" +
                        ctx.getString(R.string.maybe_call_you), Toast.LENGTH_SHORT).show();

                NotificationManagerCompat mgr = NotificationManagerCompat.from(ctx);
                mgr.notify(1, builder.build());
            }

            @Override
            public void onFailed() {

            }
        });
        crawlDataAsync.execute(number);
    }

    @Override
    protected void onIncomingCallAnswered(Context ctx, String number, Date start) {
        Log.i("hieu", "call in answered: " + number);
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
        Log.i("hieu", "call in ended: " + number);
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        Log.i("hieu", "call out started: " + number);
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
        Log.i("hieu", "call out ended: " + number);
    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start) {
        Log.i("hieu", "call missed: " + number);
    }
}
