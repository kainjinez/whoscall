package net.truonghieu.aigoido.utils.retrofit;

import android.os.AsyncTask;
import android.util.Log;

import net.truonghieu.aigoido.base.AppConstants;
import net.truonghieu.aigoido.model.PhoneNumber;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

/**
 * Created by quanghieu on 2/18/17.
 */

public class CrawlDataAsync extends AsyncTask<String, Void, Void>{
    PhoneNumber phoneNumber;
    Connection connection;
    Connection.Response response;
    public static final int STATUS_CODE_200_OK = 200;
    Document document;
    public OnCompleteListener onCompleteListener;
    String phoneNum;
    String url;

    public CrawlDataAsync(OnCompleteListener onCompleteListener) {
        super();
        this.onCompleteListener = onCompleteListener;
    }

    @Override
    protected Void doInBackground(String... params) {
        phoneNum = params[0];
        Log.i("hieu param", params[0]);
        Log.i("hieu phone", phoneNum);
        url = AppConstants.BASE_CRAWL_URL + phoneNum;
        try {
            connection = Jsoup.connect(url);
            response = connection.execute();
            if (response.statusCode() == STATUS_CODE_200_OK){
                document = response.parse();
                clearIcons(document);
                phoneNumber = new PhoneNumber();
                phoneNumber.setPhoneNumber(phoneNum);
                phoneNumber.setName(getPhoneNumber(document));
                phoneNumber.setAddress(getAddress(document));
                Log.i("hieu phonenum", phoneNumber.getPhoneNumber() + "\n" +
                phoneNumber.getName());
            }else{

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (phoneNumber != null){
            if (phoneNumber.getName().equals("")){
                onCompleteListener.onFailed();
            }else{
                onCompleteListener.onSuccess(phoneNumber);
            }
        }else{
            onCompleteListener.onFailed();
        }
        Log.i("hieu", "");
    }

    private String getAddress(Document document) {
        for (final Element e :
                document.select("div.phone-details h6.h6")) {
            return e.text().toString();
        }
        return "";
    }

    private String getPhoneNumber(Document document) {
        for (final Element e :
            document.select("div.phone-details h5.h5")) {
            return e.text().toString();
        }
        return "";
    }

    private void clearIcons(Document document) {
        for (Element e :
                document.select("i.dbdt-icon")) {
            e.remove();
        }
    }

    public interface OnCompleteListener{
        void onSuccess(PhoneNumber phoneNumber);
        void onFailed();
    }
}
