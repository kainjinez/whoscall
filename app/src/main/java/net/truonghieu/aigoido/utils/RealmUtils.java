package net.truonghieu.aigoido.utils;

import android.text.TextUtils;

import net.truonghieu.aigoido.base.App;
import net.truonghieu.aigoido.model.PhoneNumber;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by quanghieu on 4/8/17.
 */

public class RealmUtils {
    Realm realm;

    public RealmUtils() {
        this.realm = Realm.getDefaultInstance();
    }

    public Realm getRealm() {
        return realm;
    }

    public static boolean isPhoneNumberExist(String phoneNumber) {
        RealmResults<PhoneNumber> results = new RealmUtils().getRealm().where(PhoneNumber.class).equalTo("phoneNumber", phoneNumber).findAll();
        if (results.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void saveOrUpdatePhoneNumber(final PhoneNumber phoneNumber, final OnResult onResult) {
        final List<PhoneNumber> list = new ArrayList<>();
        new RealmUtils().getRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                list.add(realm.copyToRealmOrUpdate(phoneNumber));
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                onResult.onSuccess(list);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                onResult.onFailed(error);
            }
        });
    }

    public static RealmResults<PhoneNumber> getListPhoneNumbers(String field, String criteria){
        if (TextUtils.isEmpty(field)){
            return new RealmUtils().getRealm().where(PhoneNumber.class).findAllSorted("time", Sort.DESCENDING);
        }else{
            return new RealmUtils().getRealm().where(PhoneNumber.class).equalTo(field, criteria).findAllSorted("time", Sort.DESCENDING);
        }
    }



    interface OnResult{
        void onSuccess(List<PhoneNumber> listPhoneNumbers);
        void onFailed(Throwable throwable);
    }
}
