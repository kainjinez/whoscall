package net.truonghieu.aigoido.utils.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by quanghieu on 2/4/17.
 */

public class ServiceGenerator {

    public static final String BASE_URL = "http://localhost";

    private static Retrofit.Builder builderGson =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createServiceGSon(Class<S> serviceClass) {
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.serviceClassetLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        if(AppConfig.IS_DEBUG){
//            builder.addInterceptor(logging);
//        }
//        if (!SharedPreferencesHelper.getStringValue(AppConstants.DUID).equals("")){
//            builder.addInterceptor(new SettingInterceptor());
//        }SettingInterceptor
        OkHttpClient client = builder.build();
        Retrofit retrofit = builderGson.client(client).build();
        return retrofit.create(serviceClass);
    }
}
