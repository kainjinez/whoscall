package net.truonghieu.aigoido.utils.retrofit;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.content.ContentResolverCompat;
import android.support.v4.content.ContextCompat;

import net.truonghieu.aigoido.receiver.PhoneCallReceiver;

import static android.content.pm.PackageManager.*;

/**
 * Created by quanghieu on 2/18/17.
 */

public class Utils {
    public static boolean isPhoneNumberExisted(Context ctx, String number) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            return false;
        }
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor cursor = ContentResolverCompat.query(ctx.getContentResolver(), uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME},null,null,null,null);
        if (cursor.getCount() > 0){
            return true;
        }
        return false;
    }

    public static boolean isBroadcastReceiverEnabled(Context context){
        PackageManager pm = context.getPackageManager();
        ComponentName compName =
                new ComponentName(context,
                        PhoneCallReceiver.class);
        int state = pm.getComponentEnabledSetting(compName);

        return state == COMPONENT_ENABLED_STATE_ENABLED ? true : false;
    }

    public static void updateStateReceiver(Context context, boolean enable){
        PackageManager pm = context.getPackageManager();
        ComponentName compName =
                new ComponentName(context,
                        PhoneCallReceiver.class);
        pm.setComponentEnabledSetting(
                compName,
                enable ? COMPONENT_ENABLED_STATE_ENABLED : COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
