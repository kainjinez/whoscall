package net.truonghieu.aigoido.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;

import net.truonghieu.aigoido.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by quanghieu on 4/7/17.
 */

public class Utils {
    public static String getTimeInString(Context context, long time) {
        StringBuilder builder = new StringBuilder("");
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        Calendar sourceCal = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(context.getString(R.string.simpleHHMMFormat));
        builder.append(simpleDateFormat.format(c.getTime()));

        return builder.toString();
    }

    public static String getDayInString(Context context, long time) {
        StringBuilder builder = new StringBuilder("");
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        Calendar sourceCal = Calendar.getInstance();
        if (sourceCal.get(Calendar.YEAR) != c.get(Calendar.YEAR) ||
                sourceCal.get(Calendar.MONTH) != c.get(Calendar.MONTH)) {
            String s = context.getString(R.string.long_date_format);
            s = s.replace("_day", String.valueOf(c.get(Calendar.DAY_OF_MONTH)));
            s = s.replace("_month", String.valueOf(c.get(Calendar.MONTH) + 1));
            s = s.replace("_year", String.valueOf(c.get(Calendar.YEAR)));
            builder.append(s);
        } else {
            if (sourceCal.get(Calendar.DAY_OF_MONTH) == c.get(Calendar.DAY_OF_MONTH)) {
                builder.append(context.getString(R.string.today));
            } else if (sourceCal.get(Calendar.DAY_OF_MONTH) - c.get(Calendar.DAY_OF_MONTH) == 1) {
                builder.append(context.getString(R.string.yesterday));
            } else if ((sourceCal.get(Calendar.DAY_OF_MONTH) - c.get(Calendar.DAY_OF_MONTH)) < 8) {
                builder.append(sourceCal.get(Calendar.DAY_OF_MONTH) - c.get(Calendar.DAY_OF_MONTH));
                builder.append(" ");
                builder.append(context.getString(R.string.days_ago));
            } else {
                String s = context.getString(R.string.long_date_format);
                s = s.replace("_day", String.valueOf(c.get(Calendar.DAY_OF_MONTH)));
                s = s.replace("_month", String.valueOf(c.get(Calendar.MONTH) + 1));
                s = s.replace("_year", String.valueOf(c.get(Calendar.YEAR)));
                builder.append(s);
            }
        }
        return builder.toString();
    }

    public static void dialPhone(Context  context, String phone){
        context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null)));
    }

    public static void sendSMS(Context context, String phone){
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phone));
        context.startActivity(i);
    }

    public static void addContact(Context context, String phone, String name){
        Intent i = new Intent(Intent.ACTION_INSERT);
        i.setType(ContactsContract.Contacts.CONTENT_TYPE);
        i.putExtra(ContactsContract.Intents.Insert.NAME, name);
        i.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
        context.startActivity(i);
    }
}
